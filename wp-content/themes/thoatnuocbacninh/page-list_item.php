<?php /* Template Name: list item */ ?>
<?php get_header(); ?>
<?php  
    $object_current = get_queried_object();
    $meta_post = get_post_meta($object_current->ID,'layout');
    $check_layout = $meta_post[0];
?>

<div class="mh-wrapper clearfix">
    <?php if(isset($check_layout) && $check_layout == 'list') { ?>
    <div id="main-content" class="mh-loop mh-content layout_list" role="main">
        <?php if (is_active_sidebar('quan-he-co-dong')) { 
			dynamic_sidebar('quan-he-co-dong'); 
		} ?>
	</div>
    <?php }else { ?>
	<div id="main-content" class="mh-loop mh-content layout_normal" role="main"><?php
		mh_before_page_content();
		if (have_posts()) { ?>
			<header class="page-header"><?php
				the_archive_title('<h4 class="mh-widget-title cate_title"><span class="mh-widget-title-inner"><h1 class="page-title">', '</h1></span></h4>');
				if (is_author()) {
					mh_magazine_lite_author_box();
				} else {
					the_archive_description('<div class="entry-content mh-loop-description">', '</div>');
				} ?>
			</header><?php
			mh_magazine_lite_loop_layout();
			mh_magazine_lite_pagination();
		} else {
			get_template_part('content', 'none');
		} ?>
	</div>
	<?php } ?>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
