<?php /* Loop Template used for index/archive/search */ ?>
<article <?php post_class('mh-loop-item clearfix'); ?>>
	<figure class="mh-loop-thumb">
		<a href="<?php the_permalink(); ?>">
		<?php
			// if (has_post_thumbnail()) {
			// 	the_post_thumbnail('mh-magazine-lite-medium');
			// } else {
			// 	echo '<img class="mh-image-placeholder" src="' . get_template_directory_uri() . '/images/placeholder-medium.png' . '" alt="No Picture" />';
			// }

			$img_blog = get_the_post_thumbnail_url();
		?>

			<img class="mh-image-placeholder img_blog" style="background-image: url(<?php echo $img_blog; ?>);" src="<?php echo get_template_directory_uri(); ?>/images/tran_item_img.png" alt="No Picture" />
		</a>
	</figure>
	<div class="mh-loop-content clearfix">
		<header class="mh-loop-header">
			<h3 class="entry-title mh-loop-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
					<?php the_title(); ?>
				</a>
			</h3>
			<div class="mh-meta mh-loop-meta">
				<?php mh_magazine_lite_loop_meta(); ?>
			</div>
		</header>
		<div class="mh-loop-excerpt">
			<?php the_excerpt(); ?>
		</div>
	</div>
</article>