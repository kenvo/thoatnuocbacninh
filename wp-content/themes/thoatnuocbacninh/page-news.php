<?php /* Template Name: news */ ?>
<?php get_header(); ?>
<?php 
    $list_news = [
        'post_type' => 'news',
	    'posts_per_page' => 10,
        'post_status'    => 'publish',
    ];
    $GLOBALS['wp_query'] = $get_news = new WP_Query($list_news);
?>
<div class="mh-wrapper clearfix">
	<div id="main-content" class="mh-loop mh-content" role="main"><?php
		mh_before_page_content();
		if ($get_news->have_posts()) { ?>
			<header class="page-header">
				<h4 class="mh-widget-title">
			        <span class="mh-widget-title-inner">
				    <?php 
				        if (is_active_sidebar('breadcrumb')) { 
							dynamic_sidebar('breadcrumb'); 
					    } 
					?>
				    </span>
				</h4>
			    <?php
				if (is_author()) {
					mh_magazine_lite_author_box();
				} else {
					the_archive_description('<div class="entry-content mh-loop-description">', '</div>');
				} ?>
			</header>
			<?php foreach($get_news->posts as $news_val) {  
			    $image = wp_get_attachment_url(get_post_thumbnail_id($news_val->ID));
			    $link = get_permalink($news_val->ID);
			    $excerpt = get_the_excerpt($news_val->ID);

			    // echo "<pre>";
			    // var_dump($news_val);
				?>
				<article <?php post_class('mh-loop-item clearfix'); ?>>
					<figure class="mh-loop-thumb">
						<a href="<?php echo $link; ?>"><?php
							if (has_post_thumbnail($news_val->ID)) {
								echo "<img class='bg_trans' style='background-image:url(".$image.")' src='".get_template_directory_uri()."/images/tran_item_img.png' alt=''>";
							} else {
								echo '<img class="mh-image-placeholder" src="' . get_template_directory_uri() . '/images/placeholder-medium.png' . '" alt="No Picture" />';
							} ?>
						</a>
					</figure>
					<div class="mh-loop-content clearfix">
						<header class="mh-loop-header">
							<h3 class="entry-title mh-loop-title">
								<a href="<?php echo $link; ?>" rel="bookmark">
									<?php echo $news_val->post_title; ?>
								</a>
							</h3>
							<div class="mh-meta mh-loop-meta">
								<i class="fa fa-clock-o"></i><?php echo get_the_date('d/m/Y',$news_val->ID); ?>
							</div>
						</header>
						<div class="mh-loop-excerpt">
							<?php echo $excerpt; ?>
						</div>
					</div>
				</article>
			<?php
			}
			mh_magazine_lite_pagination();
		} else {
			get_template_part('content', 'none');
		} ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>

