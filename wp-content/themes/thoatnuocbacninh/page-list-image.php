<?php /* Template Name: list image */ ?>
<?php get_header(); ?>
<div class="contai_img">
    <h4 class="mh-widget-title">
        <span class="mh-widget-title-inner">
        <?php 
            if (is_active_sidebar('breadcrumb')) { 
                dynamic_sidebar('breadcrumb'); 
            } 
        ?>
        </span>
    </h4>
    <?php 
        if(is_active_sidebar('item_img')) :
           dynamic_sidebar('item_img');
        endif
     ?>
</div>
<?php get_footer(); ?>