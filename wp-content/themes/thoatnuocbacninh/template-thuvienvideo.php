<?php
/**
 * Template Name: Thư Viện Video
 *
 */

$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
$args = [
    'post_type' => 'thu_vien_video',
    'posts_per_page' => 10,
    'post_status' => ['publish'],
    'paged' => $paged
];
$gallery = new WP_Query($args);
$total_pages = $gallery->max_num_pages;

get_header(); ?>
<div class="mh-wrapper clearfix">
	<div id="main-content" class="mh-loop mh-content" role="main">
		<div class="thuvienvideo">
			<div class="row list-thuvien">
				<div class="col-md-12 col-sm-12 col-xs-12 list-images top-images pad-l-0-i pad-r-0-i">
					<div class="container">
						<div class="news-list">
							<div class="heading">
								<p>Thư viện video</p>
							</div>
						</div>
						<?php if(!empty($gallery)): ?>
						<div class="row images-row">
							<?php foreach ($gallery->posts as $key_ga => $section_image): ?>
							<?php 
								$get_video = get_field('link_youtube', $section_image->ID); 
								$get_date_event = get_field('ngay_su_kien', $section_image->ID);
								if(empty($get_date_event)) {
									$get_date_event = get_the_date( 'd/m/Y', $section_image->ID );
								}
							?>
								<div class="col-md-5 col-sm-5 col-xs-12 width-mobile" style="margin-bottom: 20px;">
									<div class="category-image">
										<?php echo $get_video; ?>
									</div>
									<p class="name-anh"><?php echo get_the_title($section_image->ID); ?></p>
									<div class="date">Ngày <?php echo $get_date_event; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="row">
							<div class="col-md-12 pagination2">
								<nav class="phantrang">
									<?php 
										echo "<div class='paginate pull-right'>";
										if ($total_pages > 1){
										    $current_page = max(1, $paged);
										    echo paginate_links(array(
										        'base' => @add_query_arg('trang','%#%'),
										        'format' => '?trang=%#%',
										        'current' => $current_page,
										        'total' => $total_pages,
										    ));
										                            
										}
										echo "</div>";
									?>
								</nav>
							</div>
						</div>
						<?php  
						else: 
							echo 'Không có dữ liệu nào !';
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>