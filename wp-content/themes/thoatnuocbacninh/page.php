<?php get_header(); ?>
<div class="mh-wrapper clearfix">
    <div id="main-content" class="mh-content" role="main" itemprop="mainContentOfPage">
    <?php
    	while (have_posts()) : the_post();
			mh_before_page_content();
			get_template_part('content', 'page');
			?>
			<div class="fb-share-button" data-href="<?php echo get_permalink(get_the_ID()); ?>" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo htmlentities(get_permalink(get_the_ID())); ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>
			<div class="vc-facebook-comment">
				<div class="fb-comments" data-href="<?php echo get_permalink(get_the_ID()); ?>" data-width="100%" data-numposts="7"></div>
			</div>
		<?php endwhile; ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>