<?php /* Template Name: Categories */ ?>
<?php get_header(); ?>
<div class="mh-wrapper clearfix">
    <div id="main-content" class="mh-content" role="main" itemprop="mainContentOfPage">
        <?php 
            if(is_active_sidebar('categories')) :
               dynamic_sidebar('categories');
            endif
         ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>