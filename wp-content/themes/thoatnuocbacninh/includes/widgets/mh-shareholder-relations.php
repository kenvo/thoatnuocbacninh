<?php

/***** MH Posts Focus [lite] *****/

class mh_shareholder_relations extends WP_Widget {
	function __construct() {
		parent::__construct(
			'mh_shareholder_relations', esc_html_x('MH Shareholder Relations', 'widget name', 'mh-magazine-lite'),
			array(
				'classname' => 'mh_shareholder_relations',
				'description' => esc_html__('MH Posts Focus widget to display 5 posts with focus on large post in the middle (if placed in full-width widget area).', 'mh-magazine-lite'),
				'customize_selective_refresh' => true
			)
		);
	}
	function widget($args, $instance) {
		$object_current = get_queried_object(); 
		if(!isset($object_current->slug)) {
			$object_current->slug = $object_current->post_name;
		}
		$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
		$defaults = array(
            'posts_per_page' => 20,
            'post_status'    => 'publish',
            'tax_query' => [
                [
                   'taxonomy' =>'category',
                   'field' => 'slug',
                   'terms' =>$object_current->slug
                ]
            ],
            'paged' => $paged
	        );
		$widget_posts = new WP_Query($defaults); 
		if(isset($object_current->post_title)) {
			$title = $object_current->post_title;
		} else {
			$title = $object_current->name;
		}
        echo $args['before_widget']; ?>
	        <h4 class="mh-widget-title">
		        <span class="mh-widget-title-inner">
			    <?php 
			        if (is_active_sidebar('breadcrumb')) { 
						dynamic_sidebar('breadcrumb'); 
				    } 
				?>
			    </span>
			</h4>
			<?php 
			if ($widget_posts->have_posts()) :
				echo '<div class="mh-row mh-posts-focus-widget clearfix">' . "\n";
					while ($widget_posts->have_posts()) : $widget_posts->the_post();
						?>
							<div class="mh-col-3-4 mh-posts-focus-wrap<?php echo esc_attr($alignment); ?> clearfix">
								<div class="mh-col-3-4 mh-posts-focus-wrap mh-posts-focus-large clearfix">
									<article class="post-<?php the_ID(); ?> mh-posts-focus-item mh-posts-focus-item-large clearfix">
										<div class="mh-meta mh-posts-focus-meta mh-posts-focus-meta-large">
											<?php mh_magazine_lite_loop_meta(); ?>
										</div>
										<div class="mh-posts-focus-title mh-posts-focus-title-large">
											<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
												<?php the_title(); ?>
											</a>
										</div>
									</article>
								</div>
						    </div>
					    <?php
					endwhile;
					wp_reset_postdata(); ?>
					<div class="wrap_paginate">
		                <div class="paginate pull-right">
			                <?php
			                $total_pages = $widget_posts->max_num_pages;

			                if ($total_pages > 1) :

			                    $current_page = max(1, $paged);

			                    echo paginate_links(array(
							        'base' => @add_query_arg('trang','%#%'),
							        'format' => '?trang=%#%',
							        'current' => $current_page,
							        'total' => $total_pages,
			                        'prev_text'    => __('<<'),
			                        'next_text'    => __('>>')
							    ));
			                ?>    
			                <?php endif; ?>
			                <?php wp_reset_postdata(); ?>
		                </div>
		            </div>
					<?php
				echo '</div>' . "\n";
			else : 
				echo "<p>Không có bài viết.</p>";
			endif;
		echo $args['after_widget'];
    }
	function update($new_instance, $old_instance) {
        $instance = array();
        if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
        if (0 !== absint($new_instance['category'])) {
			$instance['category'] = absint($new_instance['category']);
		}
		if (!empty($new_instance['tags'])) {
			$tag_slugs = explode(',', $new_instance['tags']);
			$tag_slugs = array_map('sanitize_title', $tag_slugs);
			$instance['tags'] = implode(', ', $tag_slugs);
		}
		if (0 !== absint($new_instance['offset'])) {
			if (absint($new_instance['offset']) > 50) {
				$instance['offset'] = 50;
			} else {
				$instance['offset'] = absint($new_instance['offset']);
			}
		}
        return $instance;
    }
    function form($instance) {
        $defaults = array('title' => '', 'category' => 0, 'tags' => '', 'offset' => 0);
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
		<p>
            <label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Select a Category:', 'mh-magazine-lite'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('category')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>">
            	<option value="0" <?php selected(0, $instance['category']); ?>><?php esc_html_e('All', 'mh-magazine-lite'); ?></option><?php
            		$categories = get_categories();
            		foreach ($categories as $cat) { ?>
            			<option value="<?php echo absint($cat->cat_ID); ?>" <?php selected($cat->cat_ID, $instance['category']); ?>><?php echo esc_html($cat->cat_name) . ' (' . absint($cat->category_count) . ')'; ?></option><?php
            		} ?>
            </select>
            <small><?php esc_html_e('Select a category to display posts from.', 'mh-magazine-lite'); ?></small>
		</p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('tags')); ?>"><?php esc_html_e('Filter Posts by Tags (e.g. lifestyle):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['tags']); ?>" name="<?php echo esc_attr($this->get_field_name('tags')); ?>" id="<?php echo esc_attr($this->get_field_id('tags')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('offset')); ?>"><?php esc_html_e('Skip Posts (max. 50):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo absint($instance['offset']); ?>" name="<?php echo esc_attr($this->get_field_name('offset')); ?>" id="<?php echo esc_attr($this->get_field_id('offset')); ?>" />
	    </p>
		<p>
    		<strong><?php esc_html_e('Info:', 'mh-magazine-lite'); ?></strong> <?php esc_html_e('This is the lite version of this widget with basic features. More features and options are available in the premium version of MH Magazine.', 'mh-magazine-lite'); ?>
    	</p><?php
    }
}

?>