<?php

/***** MH Posts Focus [lite] *****/

class mh_info_base extends WP_Widget {
	function __construct() {
		parent::__construct(
			'info_base_wg', esc_html_x('Info Base', 'widget name', 'mh-magazine-lite'),
			array(
				'classname' => 'mh_magazine_lite_posts_focus',
				'description' => esc_html__('show info base for your company.', 'mh-magazine-lite'),
				'customize_selective_refresh' => true
			)
		);
	}
	function widget($args, $instance) {
		$defaults = array('title' => '', 'address' => '', 'hotline' => 0, 'email' => '');
        $instance = wp_parse_args($instance, $defaults);
		$query_args = array();
		$img_fea_0 = $instance['img_fea_0'];
		if(empty($img_fea_0)) {
			$img_fea_0 = '';
		}

		$img_fea_1 = $instance['img_fea_1'];
		if(empty($img_fea_1)) {
			$img_fea_1 = '';
		}

		$img_fea_2 = $instance['img_fea_2'];
		if(empty($img_fea_2)) {
			$img_fea_2 = '';
		}

        echo $args['before_widget'];
				?>
					<div class="header_company_city">
						<div class="info_header col-xs-12 pad-l-0-i pad-r-0-i">
							<?php 
							if(!empty($instance['title'])):
							?>
							<h3><?php echo $instance['title']; ?></h3>
							<?php 
							endif;
							if(!empty($instance['address'])):
							?>
							<p class="address">Địa chỉ: <?php echo $instance['address']; ?></p>
							<?php
							endif;
							if(!empty($instance['hotline'])):
							?>
							<p class="phone">
								Điện thoại/Fax: <?php echo $instance['hotline']; ?> 
								<?php  
								if(!empty($instance['msdn'])):
								?>
								&nbsp | &nbsp <span class="msdn">Mã số doanh nghiệp: <?php echo (!empty($instance['msdn']) ? $instance['msdn'] : ""); ?></span></p>
								<?php 
								endif;
								?>
							<?php
							endif;
							if(!empty($instance['email'])):
							?>
							<p class="email">
								Email: <?php echo $instance['email']; ?> <span class="website">
								<?php  
								if(!empty($instance['website'])):
								?>
								&nbsp | &nbsp Website: <?php echo (!empty($instance['website']) ? $instance['website'] : ""); ?></span></p>
								<?php 
								endif;
								?>
							<?php 
							endif;
							?>
						</div>
						<div class="img_desc hidden-xs hidden-sm">
							<?php 
							if(!empty($img_fea_0)):
							?>
								<img class="img_fea_1" src="<?php echo $img_fea_0; ?>">
							<?php 
							endif;
							if(!empty($img_fea_1)):
							?>
								<img class="img_fea_1" src="<?php echo $img_fea_1; ?>">
							<?php 
							endif;
							if(!empty($img_fea_2)):
							?>
							<img class="img_fea_2" src="<?php echo $img_fea_2; ?>">
							<?php 
							endif;
							?>
						</div>
					</div>
				<?php
				wp_reset_postdata();
		echo $args['after_widget'];
    }
	function update($new_instance, $old_instance) {
        $instance = array();
        if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
		$instance['address'] = $new_instance['address'];
		$instance['hotline'] = $new_instance['hotline'];
		$instance['email'] = $new_instance['email'];
		$instance['img_fea_0'] = $new_instance['img_fea_0'];
		$instance['img_fea_1'] = $new_instance['img_fea_1'];
		$instance['img_fea_2'] = $new_instance['img_fea_2'];
		$instance['msdn'] = $new_instance['msdn'];
		$instance['website'] = $new_instance['website'];
        return $instance;
    }
    function form($instance) {
        $defaults = array('title' => '', 'address' => '', 'hotline' => 0, 'email' => '');
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php esc_html_e('Address', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['address']); ?>" name="<?php echo esc_attr($this->get_field_name('address')); ?>" id="<?php echo esc_attr($this->get_field_id('address')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('hotline')); ?>"><?php esc_html_e('Hotline', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['hotline']); ?>" name="<?php echo esc_attr($this->get_field_name('hotline')); ?>" id="<?php echo esc_attr($this->get_field_id('hotline')); ?>" />
	    </p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php esc_html_e('Email', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['email']); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" id="<?php echo esc_attr($this->get_field_id('email')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('img_fea_0')); ?>"><?php esc_html_e('Image feature 0', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['img_fea_0']); ?>" name="<?php echo esc_attr($this->get_field_name('img_fea_0')); ?>" id="<?php echo esc_attr($this->get_field_id('img_fea_0')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('img_fea_1')); ?>"><?php esc_html_e('Image feature 1', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['img_fea_1']); ?>" name="<?php echo esc_attr($this->get_field_name('img_fea_1')); ?>" id="<?php echo esc_attr($this->get_field_id('img_fea_1')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('img_fea_2')); ?>"><?php esc_html_e('Image feature 2', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['img_fea_2']); ?>" name="<?php echo esc_attr($this->get_field_name('img_fea_2')); ?>" id="<?php echo esc_attr($this->get_field_id('img_fea_2')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('msdn')); ?>"><?php esc_html_e('Mã số doanh nghiệp', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['msdn']); ?>" name="<?php echo esc_attr($this->get_field_name('msdn')); ?>" id="<?php echo esc_attr($this->get_field_id('msdn')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('website')); ?>"><?php esc_html_e('Website', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo ($instance['website']); ?>" name="<?php echo esc_attr($this->get_field_name('website')); ?>" id="<?php echo esc_attr($this->get_field_id('website')); ?>" />
	    </p>
    	<?php
    }
}

?>