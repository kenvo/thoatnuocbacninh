<?php

/***** List post in categories *****/

class mh_media extends WP_Widget {
	function __construct() {
		parent::__construct(
			'mh_media', esc_html_x('media', 'widget name', 'mh-magazine-lite'),
			array(
				'classname' => 'mh_media',
				'description' => esc_html__('MH media, title and meta data.', 'mh-magazine-lite'),
				'customize_selective_refresh' => true
			)
		);
	}
	function widget($args, $instance) {
		$cats_media = wp_get_nav_menu_items('Home sidebar media');
		$object_id = [];
		foreach($cats_media as $val_media) {
			$object_id[] = get_term((int)$val_media->object_id);
		}
		echo $args['before_widget'];
		    foreach($object_id as $val) { 
	        	if (function_exists('z_taxonomy_image_url')) {
	        	   $img = z_taxonomy_image_url($val->term_id);
	        	} else {
	        		$img = get_template_directory_uri() .'/images/no-thumbnail.png';
	        	}
	        	$link = get_term_link($val->term_id); 
				?>
				<h4 class="mh-widget-title">
					<span class="mh-widget-title-inner"><?php echo $val->name; ?></span>
				</h4>
				<div class="content">
					<div class="col-item-md">
					    <div class="conver">
					        <a href="<?php echo $link; ?>">
								<img class="bg_trans" src="<?php echo $img; ?>" alt="">
					        </a>
					    </div>
					</div>
			    </div>
				<?php 
		    }
	    echo $args['after_widget'];
    }
	function update($new_instance, $old_instance) {
        $instance = [];
        if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
        if (0 !== absint($new_instance['category'])) {
			$instance['category'] = absint($new_instance['category']);
		}
		if (!empty($new_instance['tags'])) {
			$tag_slugs = explode(',', $new_instance['tags']);
			$tag_slugs = array_map('sanitize_title', $tag_slugs);
			$instance['tags'] = implode(', ', $tag_slugs);
		}
		if (0 !== absint($new_instance['offset'])) {
			if (absint($new_instance['offset']) > 50) {
				$instance['offset'] = 50;
			} else {
				$instance['offset'] = absint($new_instance['offset']);
			}
		}
        return $instance;
    }
    function form($instance) {
        $defaults = [
			            'title' => '', 
			            'category' => 0, 
			            'tags' => '', 
			            'offset' => 0, 
			            'sticky' => 1
			        ];
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
		<p>
            <label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Select a Category:', 'mh-magazine-lite'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('category')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>">
            	<option value="0" <?php selected(0, $instance['category']); ?>><?php esc_html_e('All', 'mh-magazine-lite'); ?></option><?php
            		$categories = get_categories();
            		foreach ($categories as $cat) { ?>
            			<option value="<?php echo absint($cat->cat_ID); ?>" <?php selected($cat->cat_ID, $instance['category']); ?>><?php echo esc_html($cat->cat_name) . ' (' . absint($cat->category_count) . ')'; ?></option><?php
            		} ?>
            </select>
            <small><?php esc_html_e('Select a category to display posts from.', 'mh-magazine-lite'); ?></small>
		</p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('tags')); ?>"><?php esc_html_e('Filter Posts by Tags (e.g. lifestyle):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['tags']); ?>" name="<?php echo esc_attr($this->get_field_name('tags')); ?>" id="<?php echo esc_attr($this->get_field_id('tags')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('offset')); ?>"><?php esc_html_e('Skip Posts (max. 50):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo absint($instance['offset']); ?>" name="<?php echo esc_attr($this->get_field_name('offset')); ?>" id="<?php echo esc_attr($this->get_field_id('offset')); ?>" />
	    </p>
		<p>
    		<strong><?php esc_html_e('Info:', 'mh-magazine-lite'); ?></strong> <?php esc_html_e('This is the lite version of this widget with basic features. More features and options are available in the premium version of MH Magazine.', 'mh-magazine-lite'); ?>
    	</p><?php
    }
}

?>