<?php

/***** MH item image *****/

class mh_item_image_widget extends WP_Widget {
    function __construct() {
		parent::__construct(
			'mh_item_image_widget', esc_html_x('MH Item Image', 'widget name', 'mh-magazine-lite'),
			array(
				'classname' => 'mh_item_image_widget',
				'description' => esc_html__('Custom Posts Widget to display posts based on categories or tags.', 'mh-magazine-lite'),
				'customize_selective_refresh' => true
			)
		);
	}
    function widget($args, $instance) {
	    $object_current = get_queried_object();
	    $slug = $object_current->post_name;

	    $paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
        $args_pro = [
            'post_type'             => 'post',
            'post_status'           => 'publish',
            'posts_per_page' => 30,
            'tax_query' => [
                               [
                                  'taxonomy' =>'category',
                                  'field' => 'slug',
                                  'terms' =>$slug
                               ]
                           ],
            'paged' => $paged
        ];
        $get_posts = new WP_Query($args_pro);
        // var_dump($get_posts->posts);
        echo $args['before_widget'];
            foreach($get_posts->posts as $key => $get_post) {
            	if(has_post_thumbnail($get_post->ID)) {
	            	$img = wp_get_attachment_url(get_post_thumbnail_id($get_post->ID));
            	} else {
            		$img = get_template_directory_uri() .'/images/no-thumbnail.png';
            	}
            	$link = get_permalink($get_post->ID);
            	// var_dump($get_post);
	        	?>
				<div class="content">
					<div class="col-item-md">
					    <div class="conver">
					        <a href="<?php echo $link; ?>">
								<img class="bg_trans" style="background-image: url(<?php echo $img; ?>);" src="<?php echo get_template_directory_uri(); ?>/images/tran_item_img.png" alt="">
								<h3><?php echo $get_post->post_title; ?></h3>
					        </a>
					    </div>
					</div>
			    </div>
				<?php
			}?>
			<div class="wrap_paginate">
                <div class="paginate pull-right">
	                <?php
	                $total_pages = $get_posts->max_num_pages;

	                if ($total_pages > 1) :

	                    $current_page = max(1, $paged);

	                    echo paginate_links(array(
					        'base' => @add_query_arg('trang','%#%'),
					        'format' => '?trang=%#%',
					        'current' => $current_page,
					        'total' => $total_pages,
	                        'prev_text'    => __('<<'),
	                        'next_text'    => __('>>')
					    ));
	                ?>    
	                <?php endif; ?>
	                <?php wp_reset_postdata(); ?>
                </div>
            </div>
			<?php
        echo $args['after_widget'];
    }
    function update($new_instance, $old_instance) {
        $instance = array();
        if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
        if (0 !== absint($new_instance['category'])) {
			$instance['category'] = absint($new_instance['category']);
		}
		if (!empty($new_instance['tags'])) {
			$tag_slugs = explode(',', $new_instance['tags']);
			$tag_slugs = array_map('sanitize_title', $tag_slugs);
			$instance['tags'] = implode(', ', $tag_slugs);
		}
		if (0 !== absint($new_instance['postcount'])) {
			if (absint($new_instance['postcount']) > 50) {
				$instance['postcount'] = 50;
			} else {
				$instance['postcount'] = absint($new_instance['postcount']);
			}
		}
		if (0 !== absint($new_instance['offset'])) {
			if (absint($new_instance['offset']) > 50) {
				$instance['offset'] = 50;
			} else {
				$instance['offset'] = absint($new_instance['offset']);
			}
		}
		$instance['sticky'] = (!empty($new_instance['sticky'])) ? 1 : 0;
        return $instance;
    }
    function form($instance) {
	    $defaults = array('title' => '', 'category' => 0, 'tags' => '', 'postcount' => 5, 'offset' => 0, 'sticky' => 1);
        $instance = wp_parse_args($instance, $defaults); ?>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
        <p>
            <label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Select a Category:', 'mh-magazine-lite'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('category')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>">
            	<option value="0" <?php selected(0, $instance['category']); ?>><?php esc_html_e('All', 'mh-magazine-lite'); ?></option><?php
            		$categories = get_categories();
            		foreach ($categories as $cat) { ?>
            			<option value="<?php echo absint($cat->cat_ID); ?>" <?php selected($cat->cat_ID, $instance['category']); ?>><?php echo esc_html($cat->cat_name) . ' (' . absint($cat->category_count) . ')'; ?></option><?php
            		} ?>
            </select>
            <small><?php esc_html_e('Select a category to display posts from.', 'mh-magazine-lite'); ?></small>
		</p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('tags')); ?>"><?php esc_html_e('Filter Posts by Tags (e.g. lifestyle):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['tags']); ?>" name="<?php echo esc_attr($this->get_field_name('tags')); ?>" id="<?php echo esc_attr($this->get_field_id('tags')); ?>" />
	    </p>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('postcount')); ?>"><?php esc_html_e('Post Count (max. 50):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo absint($instance['postcount']); ?>" name="<?php echo esc_attr($this->get_field_name('postcount')); ?>" id="<?php echo esc_attr($this->get_field_id('postcount')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('offset')); ?>"><?php esc_html_e('Skip Posts (max. 50):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo absint($instance['offset']); ?>" name="<?php echo esc_attr($this->get_field_name('offset')); ?>" id="<?php echo esc_attr($this->get_field_id('offset')); ?>" />
	    </p>
        <p>
			<input id="<?php echo esc_attr($this->get_field_id('sticky')); ?>" name="<?php echo esc_attr($this->get_field_name('sticky')); ?>" type="checkbox" value="1" <?php checked(1, $instance['sticky']); ?> />
			<label for="<?php echo esc_attr($this->get_field_id('sticky')); ?>"><?php esc_html_e('Ignore Sticky Posts', 'mh-magazine-lite'); ?></label>
		</p>
    	<p>
    		<strong><?php esc_html_e('Info:', 'mh-magazine-lite'); ?></strong> <?php esc_html_e('This is the lite version of this widget with basic features. More features and options are available in the premium version of MH Magazine.', 'mh-magazine-lite'); ?>
    	</p><?php
    }
}

?>