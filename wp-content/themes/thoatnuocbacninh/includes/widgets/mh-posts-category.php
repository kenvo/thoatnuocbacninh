<?php

/***** List post in categories *****/

class mh_magazine_lite_posts_category extends WP_Widget {
	function __construct() {
		parent::__construct(
			'mh_magazine_lite_posts_category', esc_html_x('List posts in categories', 'widget name', 'mh-magazine-lite'),
			array(
				'classname' => 'mh_magazine_lite_posts_stacked',
				'description' => esc_html__('MH Posts Stacked widget to display 5 stacked posts nicely including thumbnail, title and meta data.', 'mh-magazine-lite'),
				'customize_selective_refresh' => true
			)
		);
	}
	function widget($args, $instance) {
		$list_cats = wp_get_nav_menu_items('Categories Home');
		$object_id = [];
		foreach($list_cats as $list_cat) {
			$object_id[] = (int)($list_cat->object_id);
		}
		foreach($object_id as $val) { 
			$get_term_by = get_terms(['include' => $val]);
			foreach($get_term_by as $key => $get_term_val) {
				$ang_post = [
	                    'posts_per_page' => 6,
	                    'post_status'    => 'publish',
	                    'tax_query' => [
	                            [
	                               'taxonomy' => $get_term_val->taxonomy,
	                               'field' => 'slug',
	                               'terms' => $get_term_val->slug
	                            ]
	                        ],
	                ];
	            $get_post = new WP_Query($ang_post);
				?>
				<div class="mh-posts-stacked-wrap mh-posts-stacked-small list_post_cat_home">
					<div class="post-<?php the_ID(); ?> mh-posts-stacked-content">
						<div class="mh-posts-stacked-thumb mh-posts-stacked-thumb-small">
						    <h4 class="mh-widget-title cate_title vc-home-post-cat">
						    	<a href="<?php echo get_category_link($get_term_val->term_id) ?>"><span class="mh-widget-title-inner vc-home-post-cat"><?php echo $get_term_val->name; ?></span></a>
						    </h4>
							<ul class="mh-custom-posts-widget clearfix ul_list_cat_home">
							    <?php
							        if(isset($get_post->posts)) {
							        	foreach($get_post->posts as $key => $post_val) {
							                $img = wp_get_attachment_url(get_post_thumbnail_id($post_val->ID));
							                $link = get_permalink($post_val->ID);
									    ?>
										    <li>
										        <figure class="mh-custom-posts-thumb">
													<a href="<?php echo $link; ?>" title="<?php the_title_attribute(); ?>"><?php
														if (has_post_thumbnail($post_val->ID)) {
															// echo get_the_post_thumbnail($post_val->ID);
															echo "<img class='bg_trans' style='background-image:url(".$img.")' src='".get_template_directory_uri()."/images/item.png' />";
														} else {
															echo '<img class="mh-image-placeholder" src="' . get_template_directory_uri() . '/images/placeholder-small.png' . '" alt="' . esc_html__('No Image', 'mh-magazine-lite') . '" />';
														} ?>
													</a>
												</figure>
											    <a class="title" href="<?php echo $link; ?>">
											       <?php echo $post_val->post_title; ?>
											    </a>
										    </li> 
										<?php 
										}
									}
								?>
							</ul>
						</div>
					</div>
				</div>
				<?php 
			}
	    }
    }
	function update($new_instance, $old_instance) {
        $instance = [];
        if (!empty($new_instance['title'])) {
			$instance['title'] = sanitize_text_field($new_instance['title']);
		}
        if (0 !== absint($new_instance['category'])) {
			$instance['category'] = absint($new_instance['category']);
		}
		if (!empty($new_instance['tags'])) {
			$tag_slugs = explode(',', $new_instance['tags']);
			$tag_slugs = array_map('sanitize_title', $tag_slugs);
			$instance['tags'] = implode(', ', $tag_slugs);
		}
		if (0 !== absint($new_instance['offset'])) {
			if (absint($new_instance['offset']) > 50) {
				$instance['offset'] = 50;
			} else {
				$instance['offset'] = absint($new_instance['offset']);
			}
		}
        return $instance;
    }
    function form($instance) {
        $defaults = [
			            'title' => '', 
			            'category' => 0, 
			            'tags' => '', 
			            'offset' => 0, 
			            'sticky' => 1
			        ];
        $instance = wp_parse_args($instance, $defaults); ?>
        <p>
        	<label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php esc_html_e('Title:', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['title']); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" id="<?php echo esc_attr($this->get_field_id('title')); ?>" />
        </p>
		<p>
            <label for="<?php echo esc_attr($this->get_field_id('category')); ?>"><?php esc_html_e('Select a Category:', 'mh-magazine-lite'); ?></label>
            <select id="<?php echo esc_attr($this->get_field_id('category')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('category')); ?>">
            	<option value="0" <?php selected(0, $instance['category']); ?>><?php esc_html_e('All', 'mh-magazine-lite'); ?></option><?php
            		$categories = get_categories();
            		foreach ($categories as $cat) { ?>
            			<option value="<?php echo absint($cat->cat_ID); ?>" <?php selected($cat->cat_ID, $instance['category']); ?>><?php echo esc_html($cat->cat_name) . ' (' . absint($cat->category_count) . ')'; ?></option><?php
            		} ?>
            </select>
            <small><?php esc_html_e('Select a category to display posts from.', 'mh-magazine-lite'); ?></small>
		</p>
		<p>
        	<label for="<?php echo esc_attr($this->get_field_id('tags')); ?>"><?php esc_html_e('Filter Posts by Tags (e.g. lifestyle):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo esc_attr($instance['tags']); ?>" name="<?php echo esc_attr($this->get_field_name('tags')); ?>" id="<?php echo esc_attr($this->get_field_id('tags')); ?>" />
	    </p>
	    <p>
        	<label for="<?php echo esc_attr($this->get_field_id('offset')); ?>"><?php esc_html_e('Skip Posts (max. 50):', 'mh-magazine-lite'); ?></label>
			<input class="widefat" type="text" value="<?php echo absint($instance['offset']); ?>" name="<?php echo esc_attr($this->get_field_name('offset')); ?>" id="<?php echo esc_attr($this->get_field_id('offset')); ?>" />
	    </p>
		<p>
    		<strong><?php esc_html_e('Info:', 'mh-magazine-lite'); ?></strong> <?php esc_html_e('This is the lite version of this widget with basic features. More features and options are available in the premium version of MH Magazine.', 'mh-magazine-lite'); ?>
    	</p><?php
    }
}

?>