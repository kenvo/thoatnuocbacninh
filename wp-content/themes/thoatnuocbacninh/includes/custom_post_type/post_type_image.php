<?php 
namespace images;

use postType\customPostType;

class PostTypeImages extends customPostType {
	public $type = 'thu_vien_anh';

    public $single = 'Thư viện ảnh';

    public $plural = 'Thư viện ảnh';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}