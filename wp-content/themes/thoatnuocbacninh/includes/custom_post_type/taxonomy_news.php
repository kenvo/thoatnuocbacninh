<?php
namespace taxonomyNews;
use taxonomy\customTaxonomy;

class TaxonomyNews extends customTaxonomy
{
    public $post_type = 'news';

    public $taxonomy_type = 'categories_news';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}

