<?php

namespace taxonomySlider;
use taxonomy\customTaxonomy;

class TaxonomySlider extends customTaxonomy
{
    public $post_type = 'slider';

    public $taxonomy_type = 'categories_slider';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}
