<?php
namespace taxonomyImages;
use taxonomy\customTaxonomy;

class TaxonomyImages extends customTaxonomy
{
    public $post_type = 'thu_vien_anh';

    public $taxonomy_type = 'Category';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}