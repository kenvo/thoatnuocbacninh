<?php 
namespace slider;

use postType\customPostType;

class PostTypeSlider extends customPostType {
	public $type = 'slider';

    public $single = 'slider';

    public $plural = 'Slider';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}