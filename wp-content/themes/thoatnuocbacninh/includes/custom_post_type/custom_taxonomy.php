<?php
namespace taxonomy;

class customTaxonomy {
	public function __construct() {
		add_action('init', [$this, 'registerTaxonomy']);
	}

	public function registerTaxonomy() {
		$result = register_taxonomy($this->taxonomy_type, $this->post_type, $this->index());

        if (is_wp_error($result)) {
            wp_die($result->get_error_message());
        };
	}

	public function index() {
		$labels = array(
		    'label' => $this->taxonomy_type,
		    'hierarchical' => true
		);
		$this->args = wp_parse_args($this->args, $labels); 

		return $this->args;
	}
}

