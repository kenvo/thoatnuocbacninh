<?php
namespace taxonomyVideo;
use taxonomy\customTaxonomy;

class TaxonomyVideo extends customTaxonomy
{
    public $post_type = 'thu_vien_video';

    public $taxonomy_type = 'Category';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}