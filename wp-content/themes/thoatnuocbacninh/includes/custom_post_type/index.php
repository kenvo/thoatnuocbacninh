<?php
require_once('custom_post_type.php');
require_once('post_type_news.php');
require_once('post_type_slider.php');
require_once('post_type_image.php');
require_once('post_type_video.php');


require_once('custom_taxonomy.php');
require_once('taxonomy_news.php');
require_once('taxonomy_slider.php');
require_once('taxonomy_image.php');
require_once('taxonomy_video.php');

use images\PostTypeImages;
use news\PostTypeNews;
use slider\PostTypeSlider;
use taxonomyImages\TaxonomyImages;
use taxonomyNews\TaxonomyNews;
use taxonomySlider\TaxonomySlider;
use taxonomyVideo\TaxonomyVideo;
use video\PostTypeVideo;

new PostTypeNews;
new PostTypeSlider;
new PostTypeImages;
new PostTypeVideo;

new TaxonomyNews;
new TaxonomySlider;
new TaxonomyImages;
new TaxonomyVideo;


