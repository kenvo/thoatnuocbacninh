<?php 
namespace news;

use postType\customPostType;

class PostTypeNews extends customPostType {
	public $type = 'news';

    public $single = 'news';

    public $plural = 'news';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}