<?php 
namespace video;

use postType\customPostType;

class PostTypeVideo extends customPostType {
	public $type = 'thu_vien_video';

    public $single = 'Thư viện video';

    public $plural = 'Thư viện video';

    public $args = ['menu_icon' => 'dashicons-format-status'];
}