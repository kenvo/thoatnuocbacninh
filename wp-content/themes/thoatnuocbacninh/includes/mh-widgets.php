<?php

/***** Register Widgets *****/

function mh_magazine_lite_register_widgets() {
	register_widget('mh_custom_posts_widget');
	register_widget('mh_slider_hp_widget');
	register_widget('mh_magazine_lite_tabbed');
	register_widget('mh_magazine_lite_posts_large');
	register_widget('mh_magazine_lite_posts_focus');
	register_widget('mh_magazine_lite_posts_stacked');
	register_widget('mh_magazine_lite_posts_category');
	register_widget('mh_link_website_local');
	register_widget('mh_link_website_ministry');
	register_widget('mh_item_image_widget');
	register_widget('mh_shareholder_relations');
	register_widget('mh_media');
	register_widget('mh_info_base');
}
add_action('widgets_init', 'mh_magazine_lite_register_widgets');

/***** Include Widgets *****/

require_once('widgets/mh-custom-posts.php');
require_once('widgets/mh-slider.php');
require_once('widgets/mh-tabbed.php');
require_once('widgets/mh-posts-large.php');
require_once('widgets/mh-posts-focus.php');
require_once('widgets/mh-posts-stacked.php');
require_once('widgets/mh-posts-category.php');
require_once('widgets/mh-link-website-local.php');
require_once('widgets/mh-link-website-ministry.php');
require_once('widgets/mh-item-image.php');
require_once('widgets/mh-shareholder-relations.php');
require_once('widgets/mh-media.php');
require_once('widgets/mh-info-base.php');

?>