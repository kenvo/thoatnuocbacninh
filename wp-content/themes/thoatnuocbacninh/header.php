<!DOCTYPE html>
<html class="no-js" <?php language_attributes();?>>
<head>
<meta charset="<?php bloginfo('charset');?>">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- <meta property="og:image"  content="<?php echo get_option('facebook_default_image'); ?>" /> -->
<meta property="fb:admins" content="<?php echo get_option('facebook_admin_id'); ?>"/>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<?php if (is_singular() && pings_open(get_queried_object())): ?>
<link rel="pingback" href="<?php bloginfo('pingback_url');?>" />
<?php endif;?>
<?php wp_head();?>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0&appId=1564265433841875&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body id="mh-mobile" <?php body_class();?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
	<div id="fb-root"></div>
<?php mh_before_header();
get_template_part('content', 'header');
mh_after_header();?>
