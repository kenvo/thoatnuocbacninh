<?php get_header(); ?>
<div class="mh-wrapper clearfix">
	<div id="main-content" class="mh-loop mh-content" role="main"><?php
		mh_before_page_content();
		if (have_posts()) { ?>
			<header class="page-header">
				<h4 class="mh-widget-title">
			        <span class="mh-widget-title-inner">
				    <?php 
				        if (is_active_sidebar('breadcrumb')) { 
							dynamic_sidebar('breadcrumb'); 
					    } 
					?>
				    </span>
				</h4>
				<?php
				if (is_author()) {
					mh_magazine_lite_author_box();
				} else {
					the_archive_description('<div class="entry-content mh-loop-description">', '</div>');
				} ?>
			</header><?php
			mh_magazine_lite_loop_layout();
			mh_magazine_lite_pagination();
		} else {
			get_template_part('content', 'none');
		} ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
