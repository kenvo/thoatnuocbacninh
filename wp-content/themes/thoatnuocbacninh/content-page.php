<?php /* Default template for displaying page content */ ?>
<article id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	    <h4 class="mh-widget-title">
	        <span class="mh-widget-title-inner">
		    <?php 
		        if (is_active_sidebar('breadcrumb')) { 
					dynamic_sidebar('breadcrumb'); 
			    } 
			?>
		    </span>
		</h4>
	</header>
	<div class="entry-content clearfix">
		<?php the_content(); ?>
	</div>
</article>