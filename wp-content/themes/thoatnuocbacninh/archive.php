<?php get_header(); ?>
<?php  
    $object_current = get_queried_object();
    $mCategory = get_category_parents($object_current->term_id);
    $parent_cat = explode('/', $mCategory)[0];
    // echo "<pre>";
    // var_dump($mCategory); die;
    $get_cat_id = get_cat_ID($parent_cat);
    $get_category = get_category($get_cat_id);

    $get_page = get_posts(array( 
	    'name' => $get_category->slug, 
	    'post_type' => 'page',
	    'post_status' => 'publish',
	    'posts_per_page' => 1));
    $meta_post = get_post_meta($get_page[0]->ID,'layout');
    $check_layout = $meta_post[0];
?>

<div class="mh-wrapper clearfix">
    <?php if(isset($check_layout) && $check_layout == 'list') { ?>
    <div id="main-content" class="mh-loop mh-content layout_list" role="main">
        <?php if (is_active_sidebar('quan-he-co-dong')) { 
			dynamic_sidebar('quan-he-co-dong'); 
		} ?>
	</div>
    <?php }else { ?>
	<div id="main-content" class="mh-loop mh-content layout_normal" role="main"><?php
		mh_before_page_content();
		if (have_posts()) { ?>
			<header class="page-header">
			<h4 class="mh-widget-title">
				<?php if(!is_category()): ?>
		        <span class="mh-widget-title-inner">
			    <?php 
			        if (is_active_sidebar('breadcrumb')) { 
						dynamic_sidebar('breadcrumb'); 
				    } 
				?>
			    </span>
			<?php endif; ?>
			</h4>
			<?php
				if (is_author()) {
					mh_magazine_lite_author_box();
				} else {
					the_archive_description('<div class="entry-content mh-loop-description">', '</div>');
				} ?>
			</header><?php
			mh_magazine_lite_loop_layout();
			mh_magazine_lite_pagination();
		} else {
			// get_template_part('content', 'none');
			echo "<span class='mh-widget-title-inner not_fout'>";
				if (is_active_sidebar('breadcrumb')) { 
					dynamic_sidebar('breadcrumb'); 
			    }
			echo "</span>";
			echo "Không có bài viết.";
		} ?>
	</div>
	<?php } ?>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
