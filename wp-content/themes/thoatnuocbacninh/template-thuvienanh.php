<?php
/**
 * Template Name: Thư Viện Ảnh
 *
 */
define('ROOT_URL', get_template_directory_uri());
$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;
$args = [
    'post_type' => 'thu_vien_anh',
    'posts_per_page' => 10,
    'post_status' => ['publish'],
    'paged' => $paged
];
$gallery = new WP_Query($args);
$total_pages = $gallery->max_num_pages;

get_header(); ?>
	<div class="mh-wrapper clearfix">
		<div id="main-content" class="mh-loop mh-content" role="main">
			<div class=" thuvienanh">
				<div class="row list-thuvien">
					<div class="col-md-12 list-images top-images pad-r-0-i pad-l-0-i">
						<div class="container">
							<div class="news-list">
								<div class="heading">
									<p>Thư viện ảnh</p>
								</div>
							</div>
							<?php if(!empty($gallery)): ?>
							<div class="row images-row">
								<?php foreach ($gallery->posts as $key_ga => $section_image): ?>
								<?php 
									$get_gallery = get_field('list_thu_vien_anh', $section_image->ID); 
									$count_gallery = count($get_gallery);
									$image_link = wp_get_attachment_url(get_post_thumbnail_id($section_image->ID));
									if(empty($image_link)) {
										$image_link = 'http://fakeimg.pl/250x150/';
									}	

									$get_date_event = get_field('ngay_su_kien', $section_image->ID);
									if(empty($get_date_event)) {
										$get_date_event = get_the_date( 'd/m/Y', $section_image->ID );
									}
								?>
									<div class="col-md-5 col-sm-5 col-xs-12 width-mobile" style="margin-bottom: 20px;">
										<div class="category-image">
											<img class="images-thuvien img-responsive" data-toggle="modal" data-target="#gallery_image_<?php echo $key_ga; ?>" style="background-image:url(<?php echo $image_link; ?>);" src="<?php echo get_template_directory_uri(); ?>/images/thu_vien_anh.png" alt="Thư Viện Ảnh">
											<div class="container">
											    <div class="modal fade" id="gallery_image_<?php echo $key_ga; ?>" role="dialog">
												    <div class="modal-dialog">
												        <div class="modal-content" >
													        <div class="modal-body">
													          	<div id="carouselExampleControls_<?php echo $key_ga; ?>" class="carousel slide" data-ride="carousel">
																    <div class="carousel-inner" role="listbox">
																    	<?php 
															    		foreach ($get_gallery as $key_sub_ga => $val_ga) :
															    		?>
																	    <div class="item <?php echo ($key_sub_ga == 0) ? 'active' : '';?>" >
																	    	<div class="" style="background-image: url('<?php echo $val_ga['url']; ?>'); background-size: cover;    background-repeat: no-repeat; background-position: center; width: 100%; height: 100%; ">
																	        	<img style="width: 100%; height: 100%;" class="d-block img-fluid img-responsive" src="<?php  echo ROOT_URL . '/images/transparent40x40.png'; ?>" alt="First slide">
																	    	</div>
																	    </div>
																		<?php endforeach; ?>
																    </div>
																    <a class="carousel-control-prev" href="#carouselExampleControls_<?php echo $key_ga; ?>" role="button" data-slide="prev">
																        <span class="carousel-control-prev-icon" aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span>
																        <span class="sr-only">Previous</span>
																    </a>
																    <a class="carousel-control-next" href="#carouselExampleControls_<?php echo $key_ga; ?>" role="button" data-slide="next">
																        <span class="carousel-control-next-icon" aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
																        <span class="sr-only">Next</span>
																    </a>
																</div>
													        </div>
													        <div class="modal-footer">
													            <button style="cursor: pointer;" type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
													        </div>
												        </div>
												    </div>
											    </div>
											</div>
											<span class="number-images text-center"><p><?php echo $count_gallery; ?> ảnh</p></span>
										</div>
										<p class="name-anh"><?php echo get_the_title($section_image->ID); ?></p>
										<div class="date">Ngày <?php echo $get_date_event; ?></div>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="row">
								<div class="col-md-12 pagination2">
									<nav class="phantrang">
										<?php 
											echo "<div class='paginate pull-right'>";
											if ($total_pages > 1){
											    $current_page = max(1, $paged);
											    echo paginate_links(array(
											        'base' => @add_query_arg('trang','%#%'),
											        'format' => '?trang=%#%',
											        'current' => $current_page,
											        'total' => $total_pages,
											    ));
											                            
											}
											echo "</div>";
										?>
									</nav>
								</div>
							</div>
							<?php  
							else: 
								echo 'Không có dữ liệu nào !';
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
<?php get_footer(); ?>