<div class="mh-header-mobile-nav clearfix"></div>
<header class="mh-header " itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="mh-container mh-container-inner mh-row clearfix hidden-xs">
		<?php mh_magazine_lite_custom_header(); ?>
	</div>
	<div class="mh-main-nav-wrap">
		<div class="hidden-sm hidden-md hidden-lg">
		<?php
			if (function_exists('the_custom_logo')) {
				the_custom_logo();
			}
		?>
		</div>
		<nav class="mh-navigation mh-main-nav mh-container mh-container-inner" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
			<?php wp_nav_menu(array('theme_location' => 'main_nav')); ?>
		</nav>
	</div>
</header>