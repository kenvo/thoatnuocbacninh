<?php
$unique_id = esc_attr( uniqid( 'search-form-' ) );
?>

<div class="vc-search-form clearfix">
	<form role="search" method="get" class="search__form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="search" id="<?php echo $unique_id; // WPCS: XSS OK. ?>" class="search__input" placeholder="<?php echo esc_attr_x( 'Tìm kiếm...', 'frontend', 'vicoder' ); ?>"  value="<?php echo get_search_query(); // WPCS: XSS OK. ?>" name="s" autocomplete="off">

		<button type="submit" class="search__button"><i class="fa fa-search"></i><span class="screen-reader-text"><?php echo _x( 'Search', 'frontend', 'vicoder' ); // WPCS: XSS OK. ?></span></button>
	</form>
</div>

