<?php get_header(); ?>
<div class="mh-wrapper clearfix">
	<div id="main-content" class="mh-content" role="main" itemprop="mainContentOfPage">
    <h4 class="mh-widget-title">
        <span class="mh-widget-title-inner">
	    <?php 
	        if (is_active_sidebar('breadcrumb')) { 
				dynamic_sidebar('breadcrumb'); 
		    } 
		?>
	    </span>
	</h4>
	<?php
		while (have_posts()) : the_post();
			mh_before_post_content();
			get_template_part('content', 'single');
			mh_after_post_content();
			include 'content-comments-facebook.php';
		endwhile; ?>
	</div>
	<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>



