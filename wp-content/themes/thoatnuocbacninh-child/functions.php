<?php

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

if (!class_exists('VicoderCustomize')) {
    /**
     * Add customizes.
     */
    class VicoderCustomize
    {

        /**
         * [__construct description].
         */
        public function __construct()
        {
            add_action('customize_register', [$this, 'customize_header']);
        }

        /**
         * [customize_header description]
         * @param  [type] $wp_customize [description]
         * @return [type]               [description]
         */
        public function customize_header($wp_customize)
        {
        	// Comment facebook.
            $wp_customize->add_section('facebook', array(
                'title'    => _x('Facebook', 'admin', 'vicoder'),
                'priority' => 25,
            ));

            // Comment facebook sdk.
            $wp_customize->add_setting('facebook_sdk', array(
                'default' => '',
                // 'transport' => 'postMessage',
            ));
            $wp_customize->add_control('facebook_sdk', array(
                'section' => 'facebook',
                'type'    => 'textarea',
                'label'   => _x('JavaScript SDK', 'admin', 'vicoder'),
            ));

            // Comment facebook html.
            $wp_customize->add_setting('facebook_html', array(
                'default' => '',
                // 'transport' => 'postMessage',
            ));

            $wp_customize->add_control('facebook_html', array(
                'section' => 'facebook',
                'type'    => 'textarea',
                'label'   => _x('Html', 'admin', 'vicoder'),
            ));

            // Header image.
            $wp_customize->add_section('header_img', array(
                'title'    => _x('Header image', 'admin', 'vicoder'),
                'priority' => 25,
            ));

            // Header image setting.
            $wp_customize->add_setting('header_img', array(
                'default' => '',
                // 'transport' => 'postMessage',
            ));

            $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'header_img', array(
                'section' => 'header_img',
                'label'   => _x('Header image', 'admin', 'vicoder'),
            )));

        }
    }
    new VicoderCustomize();
}