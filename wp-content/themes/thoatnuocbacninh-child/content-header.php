<div class="mh-header-mobile-nav clearfix"></div>
<header class="mh-header " itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="mh-container mh-container-inner mh-row clearfix">
		<a href="<?php echo esc_url(home_url('/')) ?>"><img src="<?php echo get_theme_mod('header_img') ?>" alt="<?php esc_html__('Header image', 'vicoders') ?>"></a>
	</div>
	<div class="mh-main-nav-wrap">
		<nav class="mh-navigation mh-main-nav mh-container mh-container-inner" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
			<?php wp_nav_menu(array('theme_location' => 'main_nav')); ?>
			<div class="dropdown pull-left header-search header-search--desktop">
			  <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-search"></i></button>
			  <div class="dropdown-menu dropdown-menu-right">
			    <?php get_search_form() ?>
			  </div>
			</div>
		</nav>
		<div class="header-search header-search--mobile">
			<div class="dropdown">
			  <button class="btn dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-search"></i></button>
			  <div class="dropdown-menu dropdown-menu-right">
			    <?php get_search_form() ?>
			  </div>
			</div>
		</div>
		
	</div>
</header>